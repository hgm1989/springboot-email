package com.springboot.email.email.service;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface IExportService {
    /**
     * 导出word文件到指定目录
     */
    void exportDocFile(String fileName, String tplName, Map<String, Object> data) throws Exception;
    /**
     * 导出word文件到客户端
     */
    void exportDocToClient(HttpServletResponse response, String fileName, String tplName, Map<String, Object> data) throws Exception;
}
