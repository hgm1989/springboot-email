package com.springboot.email.email.model;

import java.io.Serializable;

public class ScoreVo implements Serializable {
    private String courseName;
    private float score;
    private Integer rank;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "scoreVo{" +
                "courseName='" + courseName + '\'' +
                ", score=" + score +
                ", rank=" + rank +
                '}';
    }
}
