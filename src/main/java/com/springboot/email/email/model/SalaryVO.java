package com.springboot.email.email.model;

import java.io.Serializable;

public class SalaryVO implements Serializable {
    private Integer index;
    private String name;
    private Integer inDays;

    public SalaryVO(Integer index, String name, Integer inDays, Double baseSalary, Double reward, Double socialSecurity, Double tax, Double actSalary) {
        this.index = index;
        this.name = name;
        this.inDays = inDays;
        this.baseSalary = baseSalary;
        this.reward = reward;
        this.socialSecurity = socialSecurity;
        this.tax = tax;
        this.actSalary = actSalary;
    }

    /**
     * 基本工资
     */
    private Double baseSalary;
    /**
     * 奖金
     */
    private Double reward;
    /**
     * 社保
     */
    private Double socialSecurity;
    /**
     * 个税
     */
    private Double tax;
    private Double actSalary;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInDays() {
        return inDays;
    }

    public void setInDays(Integer inDays) {
        this.inDays = inDays;
    }

    public Double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(Double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public Double getReward() {
        return reward;
    }

    public void setReward(Double reward) {
        this.reward = reward;
    }

    public Double getSocialSecurity() {
        return socialSecurity;
    }

    public void setSocialSecurity(Double socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getActSalary() {
        return actSalary;
    }

    public void setActSalary(Double actSalary) {
        this.actSalary = actSalary;
    }

    @Override
    public String toString() {
        return "salaryVO{" +
                "index=" + index +
                ", name='" + name + '\'' +
                ", inDays=" + inDays +
                ", baseSalary=" + baseSalary +
                ", reward=" + reward +
                ", socialSecurity=" + socialSecurity +
                ", tax=" + tax +
                ", actSalary=" + actSalary +
                '}';
    }
}
