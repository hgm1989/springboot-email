package com.springboot.email.email.controller;


import com.springboot.email.email.model.ScoreVo;
import com.springboot.email.email.service.IExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/export")
public class ExportController {
    @Autowired
    private IExportService exportService;
    @RequestMapping(value = "/testWord", method= RequestMethod.GET)
    public void exportWord(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String fileName = "测试word导出.doc"; //文件名称
        // 设置头部数据
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("name","姓名");
        dataMap.put("regAddress","苏州");
        // 设置表格数据
        List<ScoreVo> list=new ArrayList<>();
        ScoreVo vo1=new ScoreVo();
        vo1.setCourseName("英语");
        vo1.setScore(95);
        vo1.setRank(3);
        ScoreVo vo2=new ScoreVo();
        vo2.setCourseName("数学");
        vo2.setScore(100);
        vo2.setRank(1);
        list.add(vo1);
        list.add(vo2);
        dataMap.put("courseList",list);
        exportService.exportDocToClient(response, fileName, "test.html", dataMap);
    }
}
