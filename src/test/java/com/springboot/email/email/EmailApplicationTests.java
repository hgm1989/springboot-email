package com.springboot.email.email;

import com.springboot.email.email.model.SalaryVO;
import com.springboot.email.email.service.IEmailService;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class EmailApplicationTests {

    @Autowired
    private IEmailService emailService;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Test
    void contextLoads() {
    }

    /**
     * 测试简单文本文件
     */
    @Test
    public void EmailTest() {
        emailService.sendSimpleMail("xxxx", "测试邮件", "springboot 邮件测试");
    }

    @Test
    public void HtmlEmailTest() throws MessagingException {
        String receiveEmail = "hgmyz@outlook.com";
        String subject = "Spring Boot 发送Html邮件测试";
        String emailContent = "<h2>您好！</h2><p>这里是一封Spring Boot 发送的邮件，祝您天天开心！<img " + "src='https://p3.toutiaoimg.com/origin/tos-cn-i-qvj2lq49k0/a43f0608912a4ecfa182084e397e4b81?from=pc' width='500' height='300' /></p>" + "<a href='https://programmerblog.xyz' title='IT技术分享设社区' targer='_blank'>IT技术分享设社区</a>";
        emailService.sendHtmlMail(receiveEmail, subject, emailContent);
    }


    @Test
    public void templateEmailTest() throws IOException, TemplateException, MessagingException {
        String receiveEmail = "xxxx";
        String subject = "Spring Boot 发送Templete邮件测试";
        //添加动态数据，替换模板里面的占位符
        SalaryVO salaryVO = new SalaryVO(1, "小明", 2, 9000.00, 350.06, 280.05, 350.00, 7806.00);
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("email.html");
        //将模板文件及数据渲染完成之后，转换为html字符串
        Map<String, Object> model = new HashMap<>();
        model.put("salary", salaryVO);
        String templateHtml = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        emailService.sendHtmlMail(receiveEmail, subject, templateHtml);
    }

    @Test
    public void emailContailAttachmentTest() throws IOException, TemplateException, MessagingException {
        String receiveEmail = "xxxx";
        String subject = "Spring Boot 发送包含附件的邮件测试";
        //添加动态数据，替换模板里面的占位符
        SalaryVO salaryVO = new SalaryVO(1, "小王", 2, 9000.00, 350.06, 280.05, 350.00, 7806.00);
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("email.html");
        //将模板文件及数据渲染完成之后，转换为html字符串
        Map<String, Object> model = new HashMap<>();
        model.put("salary", salaryVO);
        String templateHtml = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        List<String> fileList = new ArrayList<>();
        fileList.add("F:\\邮件测试.docx");
        fileList.add("F:\\5.png");
        fileList.add("F:\\db.jpg");
        emailService.sendAttachmentsMail(receiveEmail, subject, templateHtml, fileList);
    }

}
